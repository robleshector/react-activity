// Import the needed libraries
import React from "react";

import "react-bulma-components/dist/react-bulma-components.min.css";
import {
	Section,
	Heading,
	Columns
} from "react-bulma-components";

// Component Imports
import MemberAdd from './../components/MemberAdd';
import MemberList from './../components/MemberList';

class MemberPage extends React.Component {

	state = {
		message : "testing",
		status : "pending"
	}

	render() {
		const sectionStyle = {
			paddingTop: "15px",
			paddingBottom: "15px"
		}
		return(
			// Elements from the pageComponent of index.js
			<React.Fragment>
				<Section size="medium" style= {sectionStyle}>
					<Heading>Member</Heading>
					<Columns>
						<Columns.Column className="column is-3">
							<MemberAdd/>
						</Columns.Column>
						<Columns.Column className="column is-9">
							<MemberList message = {this.state.message} status= {this.state.status}/>
						</Columns.Column>
					</Columns>
				</Section>
			</React.Fragment>
		)
	}
}

export default MemberPage
// Follows the name of the file that we use