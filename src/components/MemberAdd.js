import React from 'react'

import 'react-bulma-components/dist/react-bulma-components.min.css'
import {
	Card
} from 'react-bulma-components'

class MemberAdd extends React.Component {
	render() {
		return(
			<Card>
				<Card.Header>
					<Card.Header.Title>Add Member</Card.Header.Title>
				</Card.Header>
				<Card.Content>
					<form action="#">
						<div className="field">
							<label>First Name</label>
							<div className="control">
								<input className="input" type="text"/>
							</div>
						</div>
						<div className="field">
							<label>Last Name</label>
							<div className="control">
								<input className="input" type="text"/>
							</div>
						</div>
						<div className="field">
							<label>Position Name</label>
							<div className="control">
								<input className="input" type="text"/>
							</div>
						</div>
						<div className="field">
							<label>Team</label>
							<div className="control">
							    <div className="select is-fullwidth">
							      <select>
							        <option>Select Team</option>
							        <option>Team 1</option>
							        <option>Team 2</option>
							      </select>
							    </div>
							  </div>
						</div>
						<div className="field">
							<div className="control">
								<button className="button is-success">Add</button>
							</div>
						</div>
					</form>
				</Card.Content>
			</Card>
		);
	};
};

export default MemberAdd