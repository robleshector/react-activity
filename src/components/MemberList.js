import React from 'react';

import 'react-bulma-components/dist/react-bulma-components.min.css'
import {Card} from 'react-bulma-components'

class MemberList extends React.Component {
	state = {
		members : []
	};

	componentDidMount() {
		let retrievedMembers = [
			{
				firstName: 'Juan',
				lastName: 'Dela Cruz',
				position: 'Instructor',
				team: 'Instructor Team',
			},
			{
				firstName: 'Antonio',
				lastName: 'Aguilar',
				position: 'Developer',
				team: 'Tech Team',
			}
		];

		this.setState({members : retrievedMembers})

	}

	componentDidUpdate() {
		console.log(this.state.members);
	}

	render() {
		const message = this.props.message;
		const status = this.props.status;
		return(
			<Card>
				<Card.Header>
					<Card.Header.Title>Member List</Card.Header.Title>
				</Card.Header>
				<Card.Content>
					<div className="table-container">
						<table className="table is-bordered is-fullwidth">
							<thead>
								<th>Member Name</th>
								<th>Position</th>
								<th>Team</th>
								<th>Action</th>
							</thead>
							<tbody>

								{
									this.state.members.map( member => {
										return (
												<tr>
													<td>{member.firstName} {member.lastName}</td>
													<td>{member.position}</td>
													<td>{member.team}</td>
													<td>{message} | {status}</td>
												</tr>
											)
									})
								}
								
							</tbody>
						</table>
					</div>
				</Card.Content>
			</Card>
		);
	};
};

export default MemberList