import React from 'react';
import ReactDOM from 'react-dom';

import MemberPage from "./pages/MemberPage"
import AppNavbar from "./components/AppNavbar"

// Setup the root div
const divRoot = document.getElementById('root');

// Setup the navbar
const pageComponent = (
	<React.Fragment>
		<AppNavbar/>
		<MemberPage/>
	</React.Fragment>
)

// Render the navbar
ReactDOM.render(pageComponent, divRoot)